package twitter;

import java.util.Date;

import twitter4j.*;

public class SimpleClient {

	public static void main(String[] args) throws Exception {
		
		final Twitter twitter = new TwitterFactory().getInstance();

//		/* Tasca #6 */
		TwitterStream twitterStream = TwitterStreamFactory.getSingleton();
		twitterStream.addListener(new StatusListener() {
			@Override
			public void onStatus(Status status) {
				System.out.println(status.getUser().getName() + " (@" + status.getUser().getScreenName() + "): " + status.getText());
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

			}

			@Override
			public void onTrackLimitationNotice(int i) {

			}

			@Override
			public void onScrubGeo(long l, long l1) {

			}

			@Override
			public void onStallWarning(StallWarning stallWarning) {

			}

			@Override
			public void onException(Exception e) {

			}
		});
		twitterStream.filter("#barcelona");

//		/* Tasca #5 */
//		String screenName = "fib_was";
//		ResponseList<Status> userTimeline = twitter.getUserTimeline(screenName);
//		long id = userTimeline.get(0).getId();
//		System.out.println(userTimeline.get(0).getText());
//		twitter.retweetStatus(id);

//		/* Tasca #4 */
//		Date now = new Date();
//		String latestStatus = "Hey @fib_was, we've just completed task #4 [timestamp: "+now+"]";
//		Status status = twitter.updateStatus(latestStatus);
//		System.out.println("Successfully updated the status to: " + status.getText());
	}
}
